import React from "react";
import "./App.css";

import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Paint from "./components/Paint";

function App() {
  // Creo un array de arrays

  return (
    <div className="App">
      <Paint />
    </div>
  );
}

export default App;
