import React from "react";
import { Link } from "react-router-dom";

import "../styles/Paint.scss";

export default function Paint() {
  const canvas = [];
  for (let i = 0; i < 400; i++) {
    canvas.push({ color: "#e5e5" });
  }

  const handlerChangeColor = (event) => {
    event.target.style.backgroundColor = "#fff";
  };

  return (
    <div className="Paint">
      <h1>PixelArt</h1>
      <div className="grid-container">
        {canvas.map((pixel) => (
          <div
            className="pixel"
            style={{ backgroundColor: pixel.color }}
            onClick={handlerChangeColor}
          ></div>
        ))}
      </div>
    </div>
  );
}
